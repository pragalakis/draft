# draft

A minimal Firefox extension that allows you to add notes on the new tab page.

_Information is stored in local storage so avoid sensitive information_

### Dark Theme

![Dark theme](dark-theme.png)

### Light Theme

![Light theme](light-theme.png)

## Development

First, you need to change into the example subdirectory and install all
[NodeJS](https://nodejs.org/en/) dependencies with [npm](http://npmjs.com/) or
[yarn](https://yarnpkg.com/):

    npm install

Start the continuous build process to transpile the code into something that
can run in Firefox:

    npm run dev-build

This creates a WebExtension in the `extension` subdirectory.
Any time you edit a file, it will be rebuilt automatically.

In another shell window, run the extension in Firefox using a wrapper
around [web-ext](https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Getting_started_with_web-ext):

    npm start

Any time you edit a file, [web-ext](https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Getting_started_with_web-ext) will reload the extension
in Firefox.

## Production

    npm run build

This creates a WebExtension in the `extension` subdirectory.
