import React, { useState, useEffect } from 'react';

const UnmemoizedThemeSwitch = () => {
  const [isDarkMode, setDarkMode] = useState(true);

  useEffect(() => {
    if (window.localStorage.hasOwnProperty('isDarkMode')) {
      const themeFromLocalStorage = window.localStorage.getItem('isDarkMode');
      setDarkMode(JSON.parse(themeFromLocalStorage));
    }
  }, []);

  useEffect(
    () => {
      window.localStorage.setItem('isDarkMode', isDarkMode);

      const className = 'dark';
      const bodyElement = window.document.body;
      if (isDarkMode) {
        bodyElement.classList.add(className);
      } else {
        bodyElement.classList.remove(className);
      }
    },
    [isDarkMode]
  );

  const changeTheme = event => {
    setDarkMode(!isDarkMode);
  };

  return (
    <div className="theming">
      <button className="theme" onClick={changeTheme} role="button">
        {isDarkMode ? 'dark' : 'light'}
      </button>
    </div>
  );
};

const ThemeSwitch = React.memo(UnmemoizedThemeSwitch);

export default ThemeSwitch;
