import React, { useState, useEffect } from 'react';
import ThemeSwitch from './ThemeSwitch.js';

const Draft = () => {
  const [text, setText] = useState('Type something...');

  useEffect(() => {
    if (window.localStorage.hasOwnProperty('text')) {
      const textFromLocalStorage = window.localStorage.getItem('text');
      setText(textFromLocalStorage);
    }
  }, []);

  useEffect(
    () => {
      window.localStorage.setItem('text', text);
    },
    [text]
  );

  const handleChange = event => {
    setText(event.target.value);
  };

  return (
    <div className="theme">
      <div className="container">
        <textarea className="theme" value={text} onChange={handleChange} />
      </div>

      <div className="footer">
        <ThemeSwitch />
        <div className="charcount">
          <span className="theme">
            {text.length === 1
              ? `${text.length} character`
              : `${text.length} characters`}
          </span>
        </div>
      </div>
    </div>
  );
};

export default Draft;
