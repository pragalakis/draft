import React from 'react';
import ReactDOM from 'react-dom';
import Draft from 'Draft.js';

ReactDOM.render(<Draft />, document.getElementById('app'));
